#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import Python Packages
import sys

# Import local packages
from webaudiomonitor import node


if __name__ == '__main__':
    sys.exit(node.main())
