^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rospeex_launch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.14.0 (2015-09-18)
-------------------

2.13.0 (2015-09-14)
-------------------
* Fixed launch files

2.12.6 (2015-04-24)
-------------------

2.12.5 (2015-03-03)
-------------------

2.12.4 (2015-02-19)
-------------------

2.12.3 (2015-02-13)
-------------------

2.12.2 (2015-01-07)
-------------------

2.12.1 (2014-12-26)
-------------------
* fix setupscript and package.xml
* fix ss / sr config files, and fix rospeex_launch package.
* modify package.xml
* fix comment and modify file layout.
* modify lanch files, and rospeex_web audiomonitor
* add rospeex_launch package
