#include "wavefile.h"

typedef struct {
  char           chunkID[4];
  long           chunkSize;
  short          formatTag;
  unsigned short channels;
  unsigned int   samplesPerSec;
  unsigned int   avgBytesPerSec;
  unsigned short blockAlign;
  unsigned short bitsPerSample;
} FormatChunk;

typedef struct {
  char           chunkID[4];
  unsigned int   chunkSize;
} DataChunk;

typedef struct {
  char         riff[4];
  unsigned int size;
  char         wave[4];
  FormatChunk  format;
  DataChunk    data;
} WavHeader;

#define WAV_STRING_RIFF "RIFF"
#define WAV_STRING_WAVE "WAVE"
#define WAV_STRING_FMT  "fmt "
#define WAV_STRING_DATA "data"

int WaveFile::SAMPLING_RATE = 16000;
int WaveFile::SAMPLE_BYTE = 2;

WaveFile::WaveFile()
{
}

QFile* WaveFile::createHeader(const QString &filename)
{
    QFile *file = new QFile(filename);
    if (file->open(QIODevice::WriteOnly)){
        file->seek(sizeof(WavHeader));
    }
    else{
        delete file;
        file = NULL;
    }

    return file;
}

void WaveFile::fillHeader(QFile *file, int sampleSize)
{
    file->reset();

    WavHeader header;

    memcpy(header.riff, WAV_STRING_RIFF, strlen(WAV_STRING_RIFF));
    header.size = sampleSize + sizeof(WavHeader) - 8; // filesize - 8
    memcpy(header.wave, WAV_STRING_WAVE, strlen(WAV_STRING_WAVE));

    // FormatChunk
    memcpy(header.format.chunkID, WAV_STRING_FMT, strlen(WAV_STRING_FMT));
    header.format.chunkSize = 16;
    header.format.formatTag = 1;
    header.format.channels = 1;
    header.format.samplesPerSec = SAMPLING_RATE;
    header.format.avgBytesPerSec = header.format.samplesPerSec * SAMPLE_BYTE * header.format.channels;
    header.format.blockAlign = SAMPLE_BYTE * header.format.channels;
    header.format.bitsPerSample = SAMPLE_BYTE * 8;

    // DataChunk
    memcpy(header.data.chunkID, WAV_STRING_DATA, strlen(WAV_STRING_DATA));
    header.data.chunkSize = sampleSize;

    file->write((const char*)&header, sizeof(header));
}
