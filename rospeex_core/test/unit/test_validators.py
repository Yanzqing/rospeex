#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import os
import logging
import unittest
import ConfigParser

# import locasl libraries
import rospeex_core.validators as validators
from rospeex_core import exceptions as ext

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestValidators(unittest.TestCase):
    """ rospeex.validators unittest class """

    AUDIO_LENGTH = 8000
    SUPPORTED_LANGUAGES = ['ja', 'en', 'zhe', '*']
    FRAMERATE = 16000
    CHANNELS = 1
    SAMPWIDTH = 2

    def setUp(self):
        """ setup class """
        base_dir = os.path.dirname(__file__)
        settings = ConfigParser.ConfigParser()
        filename = os.path.join(base_dir, 'config.cfg')
        settings.read(filename)
        self.flac_file = os.path.join(
            base_dir,
            settings.get('Validators', 'flac_file')
        )
        self.wav_file = os.path.join(
            base_dir,
            settings.get('Validators', 'wav_file')
        )
        self.big_wav_file = os.path.join(
            base_dir,
            settings.get('Validators', 'big_wav_file')
        )
        self.broken_wav_file = os.path.join(
            base_dir,
            settings.get('Validators', 'broken_wav_file')
        )

    def test_supported_languages(self):
        failed = False
        err_msg = ''

        # test case 01
        try:
            validators.check_language('hoge', self.SUPPORTED_LANGUAGES)
            failed = True

        except ext.UnsupportedLanguageException as err:
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'fail to validate')

        # test case 02
        try:
            validators.check_language('ja', self.SUPPORTED_LANGUAGES)

        except ext.UnsupportedLanguageException as err:
            err_msg = str(err)
            failed = True
            logger.info(err_msg)

        self.failIf(failed, 'fail to validate. Exception:%s' % err_msg)

        # test case 03
        try:
            validators.check_language('*', self.SUPPORTED_LANGUAGES)

        except ext.UnsupportedLanguageException as err:
            err_msg = str(err)
            failed = True
            logger.info(err_msg)

        self.failIf(failed, 'fail to validate. Exception:%s' % err_msg)

    def test_wave_file(self):
        failed = False
        err_msg = ''

        # broken wave
        try:
            data = open(self.broken_wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                self.CHANNELS,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'broken wave data')

        # non wave file
        try:
            data = open(self.flac_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                self.CHANNELS,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'non wave data')

        # invalid format (nchannel)
        try:
            data = open(self.wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                2,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        except Exception as err:
            failed = True
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'invalid format nchannel %s' % err_msg)

        # invalid format (framerate)
        try:
            data = open(self.wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE+1000,
                self.CHANNELS,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        except Exception as err:
            failed = True
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'invalid format framerate %s' % err_msg)

        # invalid format (sampwidth)
        try:
            data = open(self.wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                self.CHANNELS,
                self.SAMPWIDTH+2,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        except Exception as err:
            failed = True
            err_msg = str(err)
            logger.info(err_msg)
        self.failIf(failed, 'invalid format sampwidth %s' % err_msg)

        # too big wave
        try:
            data = open(self.big_wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                self.CHANNELS,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )
            failed = True

        except ext.InvalidAudioDataException as err:
            err_msg = str(err)
            logger.info(err_msg)

        except Exception as err:
            failed = True
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'too big data size sampwidth %s' % err_msg)

        # valid format
        try:
            data = open(self.wav_file, 'rb').read()
            validators.check_wave_data(
                data,
                self.FRAMERATE,
                self.CHANNELS,
                self.SAMPWIDTH,
                self.AUDIO_LENGTH
            )

        except ext.InvalidAudioDataException as err:
            failed = True
            err_msg = str(err)
            logger.info(err_msg)

        self.failIf(failed, 'invalid validate %s' % err_msg)


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(
        'rospeex_core',
        'validator',
        TestValidators,
        None,
        coverage_packages=['rospeex_core.validators']
    )
